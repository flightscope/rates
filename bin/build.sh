#!/usr/bin/env bash

set -e
set -x

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

export PATH=$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH

if ! type -p xelatex >/dev/null ; then
  >&2 echo "Missing: xelatex" >&2
fi

if ! type -p pandoc >/dev/null ; then
  >&2 echo "Missing: pandoc" >&2
fi

if ! type -p gs >/dev/null ; then
  >&2 echo "Missing: ghostscript" >&2
fi

if ! type -p convert >/dev/null ; then
  >&2 echo "Missing: imagemagick" >&2
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Missing: libreoffice" >&2
fi

if ! type -p rsync >/dev/null ; then
  >&2 echo "Missing: rsync" >&2
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
share_dir=${script_dir}/../share

mkdir -p ${dist_dir}

function makePage1 () {
  pdf_file=${1}
  pdf_basename=$(basename -- "${pdf_file}")
  pdf_dirname=$(dirname -- "${pdf_file}")
  pdf_filename="${pdf_basename%.*}"

  page1_pdf_dir=${pdf_dirname}
  page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${pdf_file}

  page1_pdf_png="${page1_pdf}.png"
  convert ${page1_pdf} ${page1_pdf_png}
}

function ensureContainsString () {
  set +e
  grep -q "${1}" "${2}"
  contains=$?
  set -e

  if [ $contains -ne "0" ]; then
    echo "${1} was not found in file ${2}"
    exit 88
  fi
}

# pre-process files
pre_src_files=$(find -L ${src_dir} -type f | sort)

aircraft_rates_dir="${src_dir}/rates/aircraft"
services_rates_dir="${src_dir}/rates/services"
merchandise_rates_dir="${src_dir}/rates/merchandise"
package_rates_dir="${src_dir}/rates/package"

Etif=$(cat "${aircraft_rates_dir}/eurofox-3k-tif.rates")
C5tif=$(cat "${aircraft_rates_dir}/cessna-152-tif.rates")
C7tif=$(cat "${aircraft_rates_dir}/cessna-172r-tif.rates")
Dtif=$(cat "${aircraft_rates_dir}/super-decathlon-tif.rates")
Xtif=$(cat "${aircraft_rates_dir}/extra-300-tif.rates")
Bdual=$(cat "${aircraft_rates_dir}/baron-55-dual.rates")
Edual=$(cat "${aircraft_rates_dir}/eurofox-3k-dual.rates")
C5dual=$(cat "${aircraft_rates_dir}/cessna-152-dual.rates")
C7dual=$(cat "${aircraft_rates_dir}/cessna-172r-dual.rates")
Pdual=$(cat "${aircraft_rates_dir}/piper-pa24-dual.rates")
Ddual=$(cat "${aircraft_rates_dir}/super-decathlon-dual.rates")
Sdual=$(cat "${aircraft_rates_dir}/seneca-pa34-200t-dual.rates")
Xdual=$(cat "${aircraft_rates_dir}/extra-300-dual.rates")
Bph=$(cat "${aircraft_rates_dir}/baron-55-private-hire.rates")
Eph=$(cat "${aircraft_rates_dir}/eurofox-3k-private-hire.rates")
C5ph=$(cat "${aircraft_rates_dir}/cessna-152-private-hire.rates")
C7ph=$(cat "${aircraft_rates_dir}/cessna-172r-private-hire.rates")
Dph=$(cat "${aircraft_rates_dir}/super-decathlon-private-hire.rates")
Sph=$(cat "${aircraft_rates_dir}/seneca-pa34-200t-private-hire.rates")
Bland=$(cat "${aircraft_rates_dir}/baron-55-landing.rates")
Eland=$(cat "${aircraft_rates_dir}/eurofox-3k-landing.rates")
C5land=$(cat "${aircraft_rates_dir}/cessna-152-landing.rates")
C7land=$(cat "${aircraft_rates_dir}/cessna-172r-landing.rates")
Pland=$(cat "${aircraft_rates_dir}/piper-pa24-landing.rates")
Dland=$(cat "${aircraft_rates_dir}/super-decathlon-landing.rates")
Sland=$(cat "${aircraft_rates_dir}/seneca-pa34-200t-landing.rates")
Xland=$(cat "${aircraft_rates_dir}/extra-300-landing.rates")
Bair=$(cat "${aircraft_rates_dir}/baron-55-airways.rates")
Eair=$(cat "${aircraft_rates_dir}/eurofox-3k-airways.rates")
C5air=$(cat "${aircraft_rates_dir}/cessna-152-airways.rates")
C7air=$(cat "${aircraft_rates_dir}/cessna-172r-airways.rates")
Pair=$(cat "${aircraft_rates_dir}/piper-pa24-airways.rates")
Dair=$(cat "${aircraft_rates_dir}/super-decathlon-airways.rates")
Sair=$(cat "${aircraft_rates_dir}/seneca-pa34-200t-airways.rates")
Xair=$(cat "${aircraft_rates_dir}/extra-300-airways.rates")
T2vd=$(cat "${aircraft_rates_dir}/tach-to-vdo.rates")
Rins=$(cat "${services_rates_dir}/raaus-instructor-hour.rates")
Gins=$(cat "${services_rates_dir}/ga-instructor-hour.rates")
Dins=$(cat "${services_rates_dir}/instructor-day.rates")
Amnt=$(cat "${services_rates_dir}/aircraft-maintenance-hour.rates")
Brf=$(cat "${services_rates_dir}/briefing-hour.rates")
Vid=$(cat "${services_rates_dir}/in-flight-video.rates")
Elp=$(cat "${services_rates_dir}/elp-assessment.rates")
Cfi=$(cat "${services_rates_dir}/cfi-surcharge-hour.rates")
Exam=$(cat "${services_rates_dir}/in-house-exam.rates")
Afr=$(cat "${services_rates_dir}/aircraft-flight-review.rates")
Asic=$(cat "${services_rates_dir}/asic-card.rates")
Bcsr=$(cat "${services_rates_dir}/cpl-bob-tait-study-reference.rates")
Bctc=$(cat "${services_rates_dir}/cpl-bob-tait-theory-course.rates")
Rts=$(cat "${services_rates_dir}/raaus-flight-test.rates")
RPLts=$(cat "${services_rates_dir}/rpl-flight-test.rates")
PPLts=$(cat "${services_rates_dir}/ppl-flight-test.rates")
CPLts=$(cat "${services_rates_dir}/cpl-flight-test.rates")
NVts=$(cat "${services_rates_dir}/nvfr-flight-test.rates")
RNts=$(cat "${services_rates_dir}/rpc-nav-flight-test.rates")
PPLpx=$(cat "${services_rates_dir}/ppl-pexo-exam.rates")
CPLpx=$(cat "${services_rates_dir}/cpl-pexo-exam.rates")
Canc=$(cat "${services_rates_dir}/cancellation-fee.rates")
mugs=$(cat "${merchandise_rates_dir}/mugs.rates")
tait=$(cat "${merchandise_rates_dir}/bob-tait-rpl-ppl-study-guide.rates")
caps=$(cat "${merchandise_rates_dir}/caps.rates")
tsht=$(cat "${merchandise_rates_dir}/t-shirts.rates")
dchs=$(cat "${merchandise_rates_dir}/dc-h10-13x-headset.rates")
stkr=$(cat "${merchandise_rates_dir}/stickers.rates")
flcm=$(cat "${merchandise_rates_dir}/jeppeson-flight-computer.rates")
fmwb=$(cat "${merchandise_rates_dir}/flying-mechanisms-workbook.rates")
acbk=$(cat "${merchandise_rates_dir}/aerocircus-book.rates")
rpcp=$(cat "${package_rates_dir}/rpc.rates")

for pre_src_file in ${pre_src_files}; do
  pre_src_file_relative=$(realpath --relative-to=${src_dir} ${pre_src_file})
  pre_src_file_relative_dirname=$(dirname ${pre_src_file_relative})
  pre_src_file_basename=$(basename -- "${pre_src_file_relative}")
  pre_src_file_extension="${pre_src_file_relative##*.}"

  dist_dir_relative=${dist_dir}/${pre_src_file_relative_dirname}
  mkdir -p ${dist_dir_relative}

  if [ ${pre_src_file_extension} = "fodt" ] || \
     [ ${pre_src_file_extension} = "fodg" ] || \
     [ ${pre_src_file_extension} = "fods" ] || \
     [ ${pre_src_file_extension} = "txt"  ] || \
     [ ${pre_src_file_extension} = "html" ]; then

    ensureContainsString "\${CI_PAGES_URL}" "${pre_src_file}"
    ensureContainsString "\${CI_PROJECT_URL}" "${pre_src_file}"
    ensureContainsString "\${COMMIT_TIME}" "${pre_src_file}"
    ensureContainsString "\${GITLAB_USER_NAME}" "${pre_src_file}"
    ensureContainsString "\${GITLAB_USER_EMAIL}" "${pre_src_file}"
    ensureContainsString "\${CI_COMMIT_SHA}" "${pre_src_file}"
    ensureContainsString "\${CI_PROJECT_VISIBILITY}" "${pre_src_file}"

    if [ ${pre_src_file_basename} = "rates.fodt" ]; then
      ensureContainsString "\${Etif}" "${pre_src_file}"
      ensureContainsString "\${C5tif}" "${pre_src_file}"
      ensureContainsString "\${C7tif}" "${pre_src_file}"
      ensureContainsString "\${Dtif}" "${pre_src_file}"
      ensureContainsString "\${Xtif}" "${pre_src_file}"
      ensureContainsString "\${Bdual}" "${pre_src_file}"
      ensureContainsString "\${Edual}" "${pre_src_file}"
      ensureContainsString "\${C5dual}" "${pre_src_file}"
      ensureContainsString "\${C7dual}" "${pre_src_file}"
      ensureContainsString "\${Pdual}" "${pre_src_file}"
      ensureContainsString "\${Ddual}" "${pre_src_file}"
      ensureContainsString "\${Sdual}" "${pre_src_file}"
      ensureContainsString "\${Xdual}" "${pre_src_file}"
      ensureContainsString "\${Bph}" "${pre_src_file}"
      ensureContainsString "\${Eph}" "${pre_src_file}"
      ensureContainsString "\${C5ph}" "${pre_src_file}"
      ensureContainsString "\${C7ph}" "${pre_src_file}"
      ensureContainsString "\${Dph}" "${pre_src_file}"
      ensureContainsString "\${Sph}" "${pre_src_file}"
      ensureContainsString "\${Bland}" "${pre_src_file}"
      ensureContainsString "\${Eland}" "${pre_src_file}"
      ensureContainsString "\${C5land}" "${pre_src_file}"
      ensureContainsString "\${C7land}" "${pre_src_file}"
      ensureContainsString "\${Pland}" "${pre_src_file}"
      ensureContainsString "\${Dland}" "${pre_src_file}"
      ensureContainsString "\${Sland}" "${pre_src_file}"
      ensureContainsString "\${Xland}" "${pre_src_file}"
      ensureContainsString "\${Bair}" "${pre_src_file}"
      ensureContainsString "\${Eair}" "${pre_src_file}"
      ensureContainsString "\${C5air}" "${pre_src_file}"
      ensureContainsString "\${C7air}" "${pre_src_file}"
      ensureContainsString "\${Pair}" "${pre_src_file}"
      ensureContainsString "\${Dair}" "${pre_src_file}"
      ensureContainsString "\${Sair}" "${pre_src_file}"
      ensureContainsString "\${Xair}" "${pre_src_file}"
      ensureContainsString "\${Rins}" "${pre_src_file}"
      ensureContainsString "\${Gins}" "${pre_src_file}"
      ensureContainsString "\${Dins}" "${pre_src_file}"
      ensureContainsString "\${Amnt}" "${pre_src_file}"
      ensureContainsString "\${Brf}" "${pre_src_file}"
      ensureContainsString "\${Vid}" "${pre_src_file}"
      ensureContainsString "\${Elp}" "${pre_src_file}"
      ensureContainsString "\${Cfi}" "${pre_src_file}"
      ensureContainsString "\${Exam}" "${pre_src_file}"
      ensureContainsString "\${Afr}" "${pre_src_file}"
      ensureContainsString "\${Rts}" "${pre_src_file}"
      ensureContainsString "\${RPLts}" "${pre_src_file}"
      ensureContainsString "\${PPLts}" "${pre_src_file}"
      ensureContainsString "\${Canc}" "${pre_src_file}"

      sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
          -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
          -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
          -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
          -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
          -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
          -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
          -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
          -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
          -e "s#\${Etif}#${Etif}#g" \
          -e "s#\${C5tif}#${C5tif}#g" \
          -e "s#\${C7tif}#${C7tif}#g" \
          -e "s#\${Dtif}#${Dtif}#g" \
          -e "s#\${Xtif}#${Xtif}#g" \
          -e "s#\${Bdual}#${Bdual}#g" \
          -e "s#\${Edual}#${Edual}#g" \
          -e "s#\${Adual}#${Adual}#g" \
          -e "s#\${C5dual}#${C5dual}#g" \
          -e "s#\${C7dual}#${C7dual}#g" \
          -e "s#\${Pdual}#${Pdual}#g" \
          -e "s#\${Ddual}#${Ddual}#g" \
          -e "s#\${Sdual}#${Sdual}#g" \
          -e "s#\${Xdual}#${Xdual}#g" \
          -e "s#\${Bph}#${Bph}#g" \
          -e "s#\${Eph}#${Eph}#g" \
          -e "s#\${Aph}#${Aph}#g" \
          -e "s#\${C5ph}#${C5ph}#g" \
          -e "s#\${C7ph}#${C7ph}#g" \
          -e "s#\${Dph}#${Dph}#g" \
          -e "s#\${Sph}#${Sph}#g" \
          -e "s#\${Bland}#${Bland}#g" \
          -e "s#\${Eland}#${Eland}#g" \
          -e "s#\${Aland}#${Aland}#g" \
          -e "s#\${C5land}#${C5land}#g" \
          -e "s#\${C7land}#${C7land}#g" \
          -e "s#\${Pland}#${Pland}#g" \
          -e "s#\${Dland}#${Dland}#g" \
          -e "s#\${Sland}#${Sland}#g" \
          -e "s#\${Xland}#${Xland}#g" \
          -e "s#\${Bair}#${Bair}#g" \
          -e "s#\${Eair}#${Eair}#g" \
          -e "s#\${Aair}#${Aair}#g" \
          -e "s#\${C5air}#${C5air}#g" \
          -e "s#\${C7air}#${C7air}#g" \
          -e "s#\${Pair}#${Pair}#g" \
          -e "s#\${Dair}#${Dair}#g" \
          -e "s#\${Sair}#${Sair}#g" \
          -e "s#\${Xair}#${Xair}#g" \
          -e "s#\${Rins}#${Rins}#g" \
          -e "s#\${Gins}#${Gins}#g" \
          -e "s#\${Dins}#${Dins}#g" \
          -e "s#\${Amnt}#${Amnt}#g" \
          -e "s#\${Brf}#${Brf}#g" \
          -e "s#\${Vid}#${Vid}#g" \
          -e "s#\${Elp}#${Elp}#g" \
          -e "s#\${Cfi}#${Cfi}#g" \
          -e "s#\${Exam}#${Exam}#g" \
          -e "s#\${Afr}#${Afr}#g" \
          -e "s#\${Rts}#${Rts}#g" \
          -e "s#\${RPLts}#${RPLts}#g" \
          -e "s#\${PPLts}#${PPLts}#g" \
          -e "s#\${Canc}#${Canc}#g" \
          ${pre_src_file} > ${dist_dir_relative}/${pre_src_file_basename}
    else
    if [ ${pre_src_file_basename} = "merchandise.fodt" ]; then
      ensureContainsString "\${mugs}" "${pre_src_file}"
      ensureContainsString "\${tait}" "${pre_src_file}"
      ensureContainsString "\${caps}" "${pre_src_file}"
      ensureContainsString "\${tsht}" "${pre_src_file}"
      ensureContainsString "\${dchs}" "${pre_src_file}"
      ensureContainsString "\${stkr}" "${pre_src_file}"
      ensureContainsString "\${flcm}" "${pre_src_file}"
      ensureContainsString "\${fmwb}" "${pre_src_file}"
      ensureContainsString "\${acbk}" "${pre_src_file}"

      sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
          -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
          -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
          -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
          -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
          -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
          -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
          -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
          -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
          -e "s#\${mugs}#${mugs}#g" \
          -e "s#\${tait}#${tait}#g" \
          -e "s#\${caps}#${caps}#g" \
          -e "s#\${tsht}#${tsht}#g" \
          -e "s#\${dchs}#${dchs}#g" \
          -e "s#\${stkr}#${stkr}#g" \
          -e "s#\${flcm}#${flcm}#g" \
          -e "s#\${fmwb}#${fmwb}#g" \
          -e "s#\${acbk}#${acbk}#g" \
          ${pre_src_file} > ${dist_dir_relative}/${pre_src_file_basename}
    else
      sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
          -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
          -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
          -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
          -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
          -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
          -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
          -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
          -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
          ${pre_src_file} > ${dist_dir_relative}/${pre_src_file_basename}
    fi
    fi
  else if [ ${pre_src_file_extension} = "md" ]; then
     (echo -e "# ${CI_PROJECT_TITLE}" && \
     echo -e "\n----\n" && \
     cat ${pre_src_file} && \
     echo -e "\n----\n" && \
     echo -e "### This document\n\n" && \
     echo -e "* hosted at **[${CI_PAGES_URL}](${CI_PAGES_URL})**\n" && \
     echo -e "* source hosted at **[${CI_PROJECT_URL}](${CI_PROJECT_URL})**\n" && \
     echo -e "* last updated at time **${COMMIT_TIME}**\n" && \
     echo -e "* last updated by user **[${GITLAB_USER_NAME}](mailto:${GITLAB_USER_EMAIL})**\n" && \
     echo -e "* revision **[${CI_COMMIT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})**\n" && \
     echo -e "* access control **${CI_PROJECT_VISIBILITY}**\n" && \
     echo -e "\n----\n" && \
     cat ${share_dir}/licence.md &&\
     echo -e "\n----\n"
    ) > ${dist_dir_relative}/${pre_src_file_basename}
  else
    rsync -aH ${pre_src_file} ${dist_dir_relative}/${pre_src_file_basename}
  fi
  fi
done

# libreoffice
libre_files=$(find -L ${dist_dir} -name '*.fodg' -o -name '*.fodt' -o -name '*.fods' -o -name '*.odg' -o -name '*.odt' -type f | sort)

for libre_file in ${libre_files}; do
  libre_file_relative=$(realpath --relative-to=${dist_dir} ${libre_file})
  libre_file_relative_dirname=$(dirname ${libre_file_relative})
  libre_file_basename=$(basename -- "${libre_file_relative}")
  libre_file_extension="${libre_file_relative##*.}"

  dist_dir_relative=${dist_dir}/${libre_file_relative_dirname}
  mkdir -p ${dist_dir_relative}

  for format in pdf html docx; do
    libreoffice --invisible --headless --convert-to ${format} ${libre_file} --outdir ${dist_dir_relative}
  done
done

# markdown
md_files=$(find -L ${dist_dir} -name '*.md' -type f | sort)

for md_file in ${md_files}; do
  md_file_relative=$(realpath --relative-to=${dist_dir} ${md_file})
  md_file_relative_dirname=$(dirname ${md_file_relative})
  md_file_basename=$(basename -- "${md_file}")
  md_file_filename="${md_file_basename%.*}"

  dist_dir_relative=${dist_dir}/${md_file_relative_dirname}
  mkdir -p ${dist_dir_relative}

  for format in pdf html docx odt; do
    pandoc -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${md_file} -o ${dist_dir_relative}/${md_file_filename}.${format}
  done
done

rsync -aH ${share_dir}/ ${dist_dir}

# page 1
pdf_files=$(find -L ${dist_dir} -name '*.pdf' -type f | sort)

for pdf_file in ${pdf_files}; do
  pdf_file_relative=$(realpath --relative-to=${dist_dir} ${pdf_file})
  pdf_file_relative_dirname=$(dirname ${pdf_file_relative})
  pdf_file_basename=$(basename -- "${pdf_file}")
  pdf_file_filename="${pdf_file_basename%.*}"

  dist_dir_relative=${dist_dir}/${pdf_file_relative_dirname}
  mkdir -p ${dist_dir_relative}

  makePage1 ${dist_dir_relative}/${pdf_file_filename}.pdf
done

# image files
img_files=$(find -L ${dist_dir} -name '*.png' -o -name '*.jpg' -o -name '*.gif' -type f | sort)

for img_file in ${img_files}; do
  img_file_extension="${img_file##*.}"

  for size in 1200 800 600 450 150 100 65
  do
    img_size="${img_file}-${size}.${img_file_extension}"
    convert ${img_file} -resize ${size}x${size} ${img_size}
  done
done
